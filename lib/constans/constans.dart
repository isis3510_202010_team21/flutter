import 'package:flutter/material.dart';

/// Tamaño del titulo de las pantallas Login y Register en el inicio.
const double kAuthTitleSize = 45.0;

/// Decoración de las entradas de texto para las ventanas de Login y Register
const kTextFieldDecoration = InputDecoration(
  hintText: 'Enter a value',
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.white, width: 1.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.white, width: 2.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
);

const kTextFieldDecorationBlack = InputDecoration(
  hintText: 'Enter a value',
  contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.black, width: 1.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.black, width: 2.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
);

/// Titulo para Login
const String kLoginTitle = 'Login';

///Titulo para Register
const String kRegisterTitle = 'Regístrate';

/// Codigo de la excepción cuando el formato del correo no es valido
const String kFirebaseEmailFormat = 'ERROR_INVALID_EMAIL';

/// Codigo de la excepción cuando el usuario no existe
const String kFirebaseEmailNotFound = 'ERROR_USER_NOT_FOUND';

/// Codigo de la excepción cuando la autenticación es invalida
const String kFirebaseWrongData = 'ERROR_WRONG_PASSWORD';

const String kFirebaseNetworkError = 'ERROR_NETWORK_REQUEST_FAILED';

/// Atributo para la URL del modelo de AR
const String kArProduct = 'product';

/// Atributo para el titulo de la ventana del modelo de AR
const String kArDetail = 'detail';

/// Permite crear un AlertDialog dado un titulo, un texto, dos opciones de respuesta validas y una imagen en el centro de manera opcional
/// @param title: Titulo de la ventana de dialogo
/// @param message: Mensaje a notificar.
/// @param type: Tipo de icono a desplegar utilizando Icons.X
/// @param imageRoute: Ruta de la imagen en el centro que se desea colocar.
/// @param onAction: Funcion a ejecutar cuando el usuario de click en la acción positiva
/// @param onCancel: Funcion a ejecutar cuando el usuario de click en la acción cancelar
/// @param labelAction: Nombre del boton para la acción positiva
/// @param labelCancel: Nombre del boton para la acción cancelar
AlertDialog kDisplayDialog(String title, String message, IconData type,
    String labelAction, Function onAction,
    [String labelCancel, Function onCancel, String imageRoute]) {
  //Muestra el dialogo de ayuda para utilizar AR
  List<Widget> content = [Text(message), SizedBox(height: 20)];
  List<Widget> actions = [
    FlatButton(
      child: Text(labelAction),
      onPressed: onAction,
    ),
  ];
  if (imageRoute != null && imageRoute.isNotEmpty) {
    content.add(Image.asset(message, alignment: Alignment.center));
  }
  if (labelCancel != null && labelCancel.isNotEmpty && onCancel != null) {
    actions.add(FlatButton(
      child: Text(labelCancel),
      onPressed: onCancel,
    ));
  }
  AlertDialog dialog = AlertDialog(
      title: Row(
        children: <Widget>[
          Icon(type),
          SizedBox(width: 20),
          Expanded(child: Text(title))
        ],
      ),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      content: Column(mainAxisSize: MainAxisSize.min, children: content),
      actions: actions);
  return dialog;
}
