import 'package:moor_flutter/moor_flutter.dart';
part 'moor_database.g.dart';

class Shoppings extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get product => text().withLength(min: 1, max: 50)();
  IntColumn get quantity => integer().withDefault(Constant(1))();
}

@UseMoor(tables: [Shoppings])
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(path: "db.sqlite"));

  @override
  int get schemaVersion => 1;

  Future<List<Shopping>> getAllShoppings() => select(shoppings).get();
  Stream<List<Shopping>> watchAllShoppings() => select(shoppings).watch();
  Future insertShopping(Insertable<Shopping> shopping) =>
      into(shoppings).insert(shopping);
  Future updateShopping(Insertable<Shopping> shopping) =>
      update(shoppings).replace(shopping);
  Future deleteShopping(Insertable<Shopping> shopping) =>
      delete(shoppings).delete(shopping);
}
