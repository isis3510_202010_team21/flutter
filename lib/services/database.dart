import 'dart:convert';

import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/models/user.dart';
import 'package:agniture_flutter/services/file_manager.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';

class DatabaseService {
  final String uid;
  DatabaseService({this.uid});

  final CollectionReference productsCollection =
      Firestore.instance.collection('products');

  DocumentReference getProductReference(String productId) =>
      productsCollection.document(productId);

  final CollectionReference usersCollection =
      Firestore.instance.collection('users');

  Stream<UserData> get userData {
    return usersCollection.document(uid).snapshots().map(_userDataFromSnapshot);
  }

  Future<void> updateUserData(String name) async {
    return await usersCollection.document(uid).setData({
      'name': name,
    });
  }

  Stream<List<Product>> get products {
    return productsCollection.snapshots().map(_productListFromDocuments);
  }

  Future<List<Product>> getProducts() async {
    final Connectivity _connectivity = Connectivity();
    ConnectivityResult connectivityResult =
        await _connectivity.checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      return ProductsStorage().getLocalProducts();
    } else {
      return productsCollection.getDocuments().then((snapshots) {
        List<Product> products = _productListFromDocuments(snapshots);
        try {
          ProductsStorage().writeProducts(jsonEncode(products));
        } catch (e) {
          print(e);
        }
        return products;
      });
    }
  }

  Stream<List<FavoriteItem>> get favorites {
    return usersCollection
        .document(uid)
        .collection('favorites')
        .snapshots()
        .map(_favoritesListFromDocuments);
  }

  Future<void> addToFavorites(String productId) async {
    await usersCollection.document(uid).collection('favorites').add({
      'product': productsCollection.document(productId),
    });
  }

  Future<void> removeFromFavorites(String productId) async {
    await usersCollection
        .document(uid)
        .collection('favorites')
        .document(productId)
        .delete();
  }

  Stream<List<ShoppingCartItem>> get shoppingCard {
    return usersCollection
        .document(uid)
        .collection('shopping')
        .snapshots()
        .map(_shoppingListFromDocuments);
  }

  Future<void> addToShoppingList(String productId, int quantity) async {
    await usersCollection.document(uid).collection('shopping').add({
      'product': productsCollection.document(productId),
      'quantity': quantity,
    });
  }

  Future<void> removeFromShoppingList(String productId) async {
    await usersCollection
        .document(uid)
        .collection('shopping')
        .document(productId)
        .delete();
  }

  Future<void> addOrder(List<OrderItemId> orderList) async {
    List orderProducts = orderList.map((product) {
      return {
        'product': productsCollection.document(product.product),
        'quantity': product.quantity
      };
    }).toList();
    await usersCollection.document(uid).collection('orders').add({
      'products': orderProducts,
    });
  }

  List<Product> _productListFromDocuments(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Product(
        id: doc.documentID,
        title: doc.data['title'] ?? '',
        description: doc.data['description'] ?? '',
        category: doc.data['category'] ?? '',
        price: doc.data['price'] ?? 0,
        image: doc.data['image'] ?? '',
        arPath: doc.data['arPath'] ?? '',
        featured: doc.data['featured'] ?? false,
      );
    }).toList();
  }

  List<FavoriteItem> _favoritesListFromDocuments(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return FavoriteItem(id: doc.documentID, product: doc.data['product']);
    }).toList();
  }

  List<ShoppingCartItem> _shoppingListFromDocuments(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return ShoppingCartItem(
          id: doc.documentID,
          quantity: doc.data['quantity'],
          product: doc.data['product']);
    }).toList();
  }

  UserData _userDataFromSnapshot(DocumentSnapshot snapshot) {
    return UserData(
      uid: uid,
      name: snapshot.data['name'],
      photo: snapshot.data['photo'],
    );
  }
}
