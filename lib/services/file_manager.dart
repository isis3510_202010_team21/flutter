import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:agniture_flutter/models/product.dart';
import 'package:path_provider/path_provider.dart';

class ProductsStorage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/products.txt');
  }

  Future<String> readProducts() async {
    try {
      final file = await _localFile;

      // Read the file
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      // If encountering an error, return 0
      return null;
    }
  }

  Future<List<Product>> getLocalProducts() async {
    return readProducts().then((json) {
      List<Product> products;
      try {
        List list = jsonDecode(json);
        products = list.map((item) => Product.fromJson(item)).toList();
      } catch (e) {
        print(e);
      }
      return products;
    });
  }

  Future<File> writeProducts(String counter) async {
    final file = await _localFile;
    // Write the file
    return file.writeAsString(counter);
  }
}
