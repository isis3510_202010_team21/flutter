// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'moor_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Shopping extends DataClass implements Insertable<Shopping> {
  final int id;
  final String product;
  final int quantity;
  Shopping(
      {@required this.id, @required this.product, @required this.quantity});
  factory Shopping.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return Shopping(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      product:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}product']),
      quantity:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}quantity']),
    );
  }
  factory Shopping.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Shopping(
      id: serializer.fromJson<int>(json['id']),
      product: serializer.fromJson<String>(json['product']),
      quantity: serializer.fromJson<int>(json['quantity']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'product': serializer.toJson<String>(product),
      'quantity': serializer.toJson<int>(quantity),
    };
  }

  @override
  ShoppingsCompanion createCompanion(bool nullToAbsent) {
    return ShoppingsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      product: product == null && nullToAbsent
          ? const Value.absent()
          : Value(product),
      quantity: quantity == null && nullToAbsent
          ? const Value.absent()
          : Value(quantity),
    );
  }

  Shopping copyWith({int id, String product, int quantity}) => Shopping(
        id: id ?? this.id,
        product: product ?? this.product,
        quantity: quantity ?? this.quantity,
      );
  @override
  String toString() {
    return (StringBuffer('Shopping(')
          ..write('id: $id, ')
          ..write('product: $product, ')
          ..write('quantity: $quantity')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(product.hashCode, quantity.hashCode)));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Shopping &&
          other.id == this.id &&
          other.product == this.product &&
          other.quantity == this.quantity);
}

class ShoppingsCompanion extends UpdateCompanion<Shopping> {
  final Value<int> id;
  final Value<String> product;
  final Value<int> quantity;
  const ShoppingsCompanion({
    this.id = const Value.absent(),
    this.product = const Value.absent(),
    this.quantity = const Value.absent(),
  });
  ShoppingsCompanion.insert({
    this.id = const Value.absent(),
    @required String product,
    this.quantity = const Value.absent(),
  }) : product = Value(product);
  ShoppingsCompanion copyWith(
      {Value<int> id, Value<String> product, Value<int> quantity}) {
    return ShoppingsCompanion(
      id: id ?? this.id,
      product: product ?? this.product,
      quantity: quantity ?? this.quantity,
    );
  }
}

class $ShoppingsTable extends Shoppings
    with TableInfo<$ShoppingsTable, Shopping> {
  final GeneratedDatabase _db;
  final String _alias;
  $ShoppingsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _productMeta = const VerificationMeta('product');
  GeneratedTextColumn _product;
  @override
  GeneratedTextColumn get product => _product ??= _constructProduct();
  GeneratedTextColumn _constructProduct() {
    return GeneratedTextColumn('product', $tableName, false,
        minTextLength: 1, maxTextLength: 50);
  }

  final VerificationMeta _quantityMeta = const VerificationMeta('quantity');
  GeneratedIntColumn _quantity;
  @override
  GeneratedIntColumn get quantity => _quantity ??= _constructQuantity();
  GeneratedIntColumn _constructQuantity() {
    return GeneratedIntColumn('quantity', $tableName, false,
        defaultValue: Constant(1));
  }

  @override
  List<GeneratedColumn> get $columns => [id, product, quantity];
  @override
  $ShoppingsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'shoppings';
  @override
  final String actualTableName = 'shoppings';
  @override
  VerificationContext validateIntegrity(ShoppingsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.product.present) {
      context.handle(_productMeta,
          product.isAcceptableValue(d.product.value, _productMeta));
    } else if (isInserting) {
      context.missing(_productMeta);
    }
    if (d.quantity.present) {
      context.handle(_quantityMeta,
          quantity.isAcceptableValue(d.quantity.value, _quantityMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Shopping map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Shopping.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(ShoppingsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.product.present) {
      map['product'] = Variable<String, StringType>(d.product.value);
    }
    if (d.quantity.present) {
      map['quantity'] = Variable<int, IntType>(d.quantity.value);
    }
    return map;
  }

  @override
  $ShoppingsTable createAlias(String alias) {
    return $ShoppingsTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $ShoppingsTable _shoppings;
  $ShoppingsTable get shoppings => _shoppings ??= $ShoppingsTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [shoppings];
}
