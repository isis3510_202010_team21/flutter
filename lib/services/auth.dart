import 'package:agniture_flutter/models/user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthService {
  /// Instancia de Firebase.
  final _auth = FirebaseAuth.instance;

  /// Instancia de Googla OAuth
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  User _userFromFirebaseUser(FirebaseUser user) {
    User pUser = user != null
        ? User(
            uid: user.uid,
            name: user.displayName,
            photo: user.photoUrl,
            email: user.email)
        : null;
    print("[Usuario Loggeado] ${pUser.toString()}");
    return pUser;
  }

  Stream<User> get user {
    return _auth.onAuthStateChanged.map(_userFromFirebaseUser);
  }

  /// Permite registrar un usuario en la base de datos de Firebase
  Future registerUser(String email, String password, String name) async {
    final newUser = await _auth.createUserWithEmailAndPassword(
        email: email, password: password);
    FirebaseUser user = newUser.user;
    UserUpdateInfo updateUser = new UserUpdateInfo();
    updateUser.displayName = name;
    user.updateProfile(updateUser);
    return _userFromFirebaseUser(user);
  }

  /// Permite recuperar la información de un usuario de la base de datos Firebase
  Future loginUser(String email, String password, String name) async {
    AuthResult newUser = await _auth.signInWithEmailAndPassword(
        email: email, password: password);
    FirebaseUser user = newUser.user;
    return _userFromFirebaseUser(user);
  }

  Future<User> googleAuth() async {
    try {
      print('[Google] Autenticación por Google');
      final GoogleSignInAccount googleSignInAccount =
          await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleSignInAccount.authentication;
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      AuthResult authResult = await _auth.signInWithCredential(credential);
      return _userFromFirebaseUser(authResult.user);
    } catch (e) {
      print('[Google Auth] Error en el proceso');
      print(e);
      print(e.message);
    }
    return null;
  }

  // sign in anon
  Future signInAnon() async {
    AuthResult result = await _auth.signInAnonymously();
    FirebaseUser user = result.user;
    return _userFromFirebaseUser(user);
  }

  // sign out
  Future signOut() async {
    try {
      await _auth.signOut();
      await _googleSignIn.signOut();
      return;
    } catch (error) {
      print(error.toString());
      return null;
    }
  }
}
