import 'package:cloud_firestore/cloud_firestore.dart';

class Product {
  final String id;
  final String title;
  final String description;
  final String category;
  final int price;
  final String image;
  final String arPath;
  final bool featured;
  Product(
      {this.id,
      this.title,
      this.description,
      this.category,
      this.price,
      this.image,
      this.arPath,
      this.featured});

  Product.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'],
        description = json['description'],
        category = json['category'],
        price = json['price'],
        image = json['image'],
        arPath = json['arPath'],
        featured = json['featured'];
  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'description': description,
        'category': category,
        'price': price,
        'image': image,
        'arPath': arPath,
        'featured': featured,
      };
}

class ProductCategory {
  final String key;
  final String label;
  final String icon;
  ProductCategory({this.key, this.label, this.icon});
}

class FavoriteItem {
  final String id;
  final DocumentReference product;
  FavoriteItem({this.id, this.product});
}

class FavoriteProduct {
  final String id;
  final Product product;
  FavoriteProduct({this.id, this.product});
}

class ShoppingCartItem {
  final String id;
  final int quantity;
  final DocumentReference product;
  ShoppingCartItem({this.id, this.quantity, this.product});
}

class ShoppingCartProduct {
  final String id;
  final int quantity;
  final Product product;
  ShoppingCartProduct({this.id, this.quantity, this.product});
}

class OrderItem {
  final String id;
  final int quantity;
  final DocumentReference product;
  OrderItem({this.id, this.quantity, this.product});
}

class OrderProduct {
  final String id;
  final int quantity;
  final Product product;
  OrderProduct({this.id, this.quantity, this.product});
}

class OrderItemId {
  final int quantity;
  final String product;
  OrderItemId({this.quantity, this.product});
}
