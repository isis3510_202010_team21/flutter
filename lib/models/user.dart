/// Modelos para el usuario.

class User {
  final String uid;
  final String name;
  final String photo;
  final String email;
  User({this.uid, this.name, this.photo, this.email});

  @override
  String toString() {
    return "UID: ${this.uid} - Nombre: ${this.name} - Photo: ${this.photo} - Email: ${this.email}";
  }
}

class UserData {
  final String uid;
  final String name;
  final String photo;
  UserData({this.uid, this.name, this.photo});
}
