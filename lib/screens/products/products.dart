import 'package:agniture_flutter/screens/products/products_list.dart';
import 'package:flutter/material.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/components/ar_floating_button/ar_floating_button.dart';
import 'package:agniture_flutter/components/navigation_bar/navigation_bar.dart';

class ProductsScreenArguments {
  final ProductCategory category;
  ProductsScreenArguments(
    this.category,
  );
}

class Products extends StatelessWidget {
  Products({Key key}) : super(key: key);
  static const String id = '/products';

  @override
  Widget build(BuildContext context) {
    final ProductsScreenArguments args =
        ModalRoute.of(context).settings.arguments;

    ProductCategory category = args.category;

    return Scaffold(
      appBar: AppBar(
        title: Text(category.label,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0)),
      ),
      body: ProductsList(category: category),
      bottomNavigationBar: NavigationBar(id: id),
      floatingActionButton: ARFloatingButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
