import 'package:agniture_flutter/screens/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:agniture_flutter/screens/product_detail/product_detail.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/screens/loading/loading.dart';
import 'package:provider/provider.dart';

class ProductsList extends StatelessWidget {
  const ProductsList({Key key, this.category}) : super(key: key);
  final ProductCategory category;
  @override
  Widget build(BuildContext context) {
    List<Product> products = Provider.of<List<Product>>(context);
    List<Product> productsByCategory = category != null
        ? products
            .where(
              (product) => product.category == category.key,
            )
            .toList()
        : products;

    return products == null
        ? Loading()
        : ListView(
            padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
            children: productsByCategory.length > 0
                ? productsByCategory.map<Widget>((Product product) {
                    return Container(
                      margin: const EdgeInsets.only(bottom: 8.0),
                      child: ProductItem(product: product),
                    );
                  }).toList()
                : [
                    Text(
                      "No hay producto de esta categoria",
                      textAlign: TextAlign.center,
                    )
                  ],
          );
  }
}

class ProductItem extends StatelessWidget {
  const ProductItem({Key key, @required this.product})
      : assert(product != null),
        super(key: key);
  final Product product;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        App.analytics.logEvent(name: "view_item", properties: {
          'event_properties': {
            'itemId': product.id,
            'itemName': product.title,
            'itemCategory': product.category,
            'origin': "Products List",
          }
        });

        Navigator.pushNamed(
          context,
          ProductDetail.id,
          arguments: DetailScreenArguments(product),
        );
      },
      child: Container(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 2, horizontal: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: SizedBox(
                  child: CachedNetworkImage(
                      imageUrl: product.image,
                      fit: BoxFit.fitWidth,
                      height: 180.0,
                      width: 500.0),
                ),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 8.0),
                  child: Text(product.title))
            ],
          ),
        ),
      ),
    );
  }
}
