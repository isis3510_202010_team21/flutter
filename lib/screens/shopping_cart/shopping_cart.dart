import 'package:agniture_flutter/screens/shopping_cart/database_shopping.dart';
import 'package:flutter/material.dart';
import 'package:agniture_flutter/components/navigation_bar/navigation_bar.dart';
import 'package:agniture_flutter/components/ar_floating_button/ar_floating_button.dart';

class ShoppingCart extends StatelessWidget {
  static const String id = '/shopping_cart';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Carrito de compras',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0)),
      ),
      body: DatabaseShopping(),
      bottomNavigationBar: NavigationBar(id: id),
      floatingActionButton: ARFloatingButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
