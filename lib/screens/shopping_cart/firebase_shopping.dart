import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/screens/product_detail/product_detail.dart';
import 'package:agniture_flutter/screens/loading/loading.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class FirebaseShopping extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Product> products = Provider.of<List<Product>>(context);
    List<ShoppingCartProduct> shoppingList =
        _referenceToModelList(products, []);
    bool loading = products == null;
    return loading
        ? Loading()
        : ListView(
            padding: const EdgeInsets.only(top: 8.0, left: 20.0, right: 20.0),
            children: shoppingList.map<Widget>((ShoppingCartProduct shopping) {
              return Container(
                margin: const EdgeInsets.only(bottom: 8.0),
                child: ShoppingItem(shoppingItem: shopping),
              );
            }).toList(),
          );
  }

  List<ShoppingCartProduct> _referenceToModelList(
      List<Product> allProducts, List<ShoppingCartItem> references) {
    List<ShoppingCartProduct> result = [];
    references.forEach((shoppingItem) {
      Product product = allProducts.firstWhere(
          (product) => product.id == shoppingItem.product.documentID,
          orElse: () => null);
      if (product != null) {
        result.add(ShoppingCartProduct(id: shoppingItem.id, product: product));
      }
    });
    return result;
  }
}

class ShoppingItem extends StatelessWidget {
  const ShoppingItem({Key key, @required this.shoppingItem})
      : assert(shoppingItem != null),
        super(key: key);
  final ShoppingCartProduct shoppingItem;

  @override
  Widget build(BuildContext context) {
    NumberFormat currency = NumberFormat.simpleCurrency();
    return Column(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                spreadRadius: 0.5,
                blurRadius: 8.0,
                offset: Offset(3.0, 5.0),
              ),
            ],
          ),
          child: Padding(
            padding:
                EdgeInsets.only(top: 15.0, right: 0, bottom: 15.0, left: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    child: InkWell(
                        onTap: () {
                          Navigator.pushNamed(
                            context,
                            ProductDetail.id,
                            arguments:
                                DetailScreenArguments(shoppingItem.product),
                          );
                        },
                        child: Text(shoppingItem.product.title,
                            overflow: TextOverflow.fade,
                            style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black87)))),
                Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Row(
                      children: <Widget>[
                        IconButton(
                            icon: Icon(
                              Icons.remove_circle,
                              color: Colors.black,
                            ),
                            onPressed: null),
                        Text(shoppingItem.quantity.toString(),
                            style: TextStyle(
                              fontSize: 16.0,
                            )),
                        IconButton(
                            icon: Icon(
                              Icons.add_circle,
                              color: Colors.black,
                            ),
                            onPressed: null),
                      ],
                    ))
              ],
            ),
          ),
        ),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Colors.black,
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Precio',
                          style:
                              TextStyle(fontSize: 16.0, color: Colors.white)),
                      Text(currency.format(30000),
                          style: TextStyle(fontSize: 16.0, color: Colors.white))
                    ],
                  ),
                )))
      ],
    );
  }
}
