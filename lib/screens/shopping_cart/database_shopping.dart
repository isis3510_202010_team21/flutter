import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:provider/provider.dart';
import 'package:agniture_flutter/constans/constans.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/screens/loading/loading.dart';
import 'package:agniture_flutter/services/moor_database.dart';
import 'package:agniture_flutter/screens/auth/auth.dart';
import 'package:agniture_flutter/models/user.dart';
import 'package:agniture_flutter/screens/product_detail/product_detail.dart';
import 'package:agniture_flutter/screens/shopping_summary/shopping_summary.dart';

class DatabaseShopping extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Product> products = Provider.of<List<Product>>(context);
    AppDatabase database = Provider.of<AppDatabase>(context);
    bool loading = products == null || database == null;
    return loading
        ? Loading()
        : StreamBuilder(
            stream: database.watchAllShoppings(),
            builder: (context, AsyncSnapshot<List<Shopping>> snapshot) {
              final shoppings = snapshot.data ?? [];

              return ListView(
                padding:
                    const EdgeInsets.only(top: 8.0, left: 20.0, right: 20.0),
                children: _listItemsRender(shoppings, products, database),
              );
            });
  }

  List<Widget> _listItemsRender(shoppings, products, database) {
    List<Widget> listItems = [];
    shoppings.forEach((Shopping shopping) {
      ShoppingCartProduct shoppingCartProduct =
          _tableToModelProduct(products, shopping);
      if (shoppingCartProduct != null) {
        listItems.add(Container(
          margin: const EdgeInsets.only(bottom: 8.0),
          child: ShoppingItem(
              shoppingItem: shoppingCartProduct,
              shoppingRef: shopping,
              database: database),
        ));
      }
    });

    listItems.add(ShoppingButton());
    return listItems.length > 1
        ? listItems
        : [
            Text(
              "El carrito de compras está vacío",
              textAlign: TextAlign.center,
            )
          ];
  }

  ShoppingCartProduct _tableToModelProduct(
      List<Product> allProducts, Shopping shopping) {
    Product product = allProducts.firstWhere(
        (product) => product.id == shopping.product,
        orElse: () => null);
    if (product != null) {
      return ShoppingCartProduct(
          id: null, product: product, quantity: shopping.quantity);
    }
    return null;
  }
}

class ShoppingItem extends StatelessWidget {
  const ShoppingItem(
      {Key key,
      @required this.shoppingItem,
      @required this.shoppingRef,
      @required this.database})
      : assert(shoppingItem != null),
        assert(shoppingRef != null),
        assert(database != null),
        super(key: key);
  final ShoppingCartProduct shoppingItem;
  final Shopping shoppingRef;
  final AppDatabase database;

  Future<void> _addToShoppingCart() async {
    return database.updateShopping(
        shoppingRef.copyWith(quantity: shoppingRef.quantity + 1));
  }

  Future<void> _removeFromShoppingCart() async {
    int newQuantity = shoppingRef.quantity - 1;
    if (newQuantity > 0) {
      return database
          .updateShopping(shoppingRef.copyWith(quantity: newQuantity));
    } else {
      return database.deleteShopping(shoppingRef);
    }
  }

  @override
  Widget build(BuildContext context) {
    NumberFormat currency = NumberFormat.simpleCurrency();
    return Column(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                spreadRadius: 0.5,
                blurRadius: 8.0,
                offset: Offset(3.0, 5.0),
              ),
            ],
          ),
          child: Padding(
            padding:
                EdgeInsets.only(top: 15.0, right: 0, bottom: 15.0, left: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    child: InkWell(
                        onTap: () {
                          Navigator.pushNamed(
                            context,
                            ProductDetail.id,
                            arguments:
                                DetailScreenArguments(shoppingItem.product),
                          );
                        },
                        child: Text(shoppingItem.product.title,
                            overflow: TextOverflow.fade,
                            style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black87)))),
                Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Row(
                      children: <Widget>[
                        IconButton(
                            icon: Icon(
                              Icons.remove_circle,
                              color: Colors.black,
                            ),
                            onPressed: _removeFromShoppingCart),
                        Text(shoppingItem.quantity.toString(),
                            style: TextStyle(
                              fontSize: 16.0,
                            )),
                        IconButton(
                          icon: Icon(
                            Icons.add_circle,
                            color: Colors.black,
                          ),
                          onPressed: _addToShoppingCart,
                        )
                      ],
                    ))
              ],
            ),
          ),
        ),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Colors.black,
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Precio',
                          style:
                              TextStyle(fontSize: 16.0, color: Colors.white)),
                      Text(currency.format(shoppingItem.product.price),
                          style: TextStyle(fontSize: 16.0, color: Colors.white))
                    ],
                  ),
                )))
      ],
    );
  }
}

class ShoppingButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    return GestureDetector(
      onTap: () {
        if (user.email == null || user.email == '') {
          Navigator.pushNamed(context, Login.id, arguments: kLoginTitle);
        } else {
          Navigator.pushNamed(context, ShoppingSummary.id);
        }
      },
      child: Container(
          padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.black,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Center(
                child: Text(
                  "CONTINUAR",
                  style: TextStyle(fontSize: 20.0, color: Colors.white),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white,
                        size: 18,
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white,
                        size: 18,
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white,
                        size: 18,
                      ),
                    ],
                  )),
            ],
          )),
    );
  }
}
