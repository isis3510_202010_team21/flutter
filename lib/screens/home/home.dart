import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agniture_flutter/components/app_drawer/app_drawer.dart';
import 'package:agniture_flutter/screens/home/category_list.dart';
import 'package:agniture_flutter/screens/home/featured_list.dart';
import 'package:agniture_flutter/components/navigation_bar/navigation_bar.dart';
import 'package:agniture_flutter/components/ar_floating_button/ar_floating_button.dart';
import 'package:agniture_flutter/components/user_icon/user_icon.dart';
import 'package:agniture_flutter/models/user.dart';

const String featuredTab = 'Destacado';
const String categoriesTab = 'Categorías';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);
  static const String id = '/home';

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  final List<Tab> myTabs = <Tab>[
    Tab(text: featuredTab),
    Tab(text: categoriesTab),
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: myTabs.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Inicio',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0)),
        actions: user.email != null && user.email != ''
            ? <Widget>[
                Padding(
                    padding: EdgeInsets.only(
                      right: 15.0,
                    ),
                    child: UserIcon(executeFunction: this.dispose))
              ]
            : null,
        bottom: TabBar(
          controller: _tabController,
          tabs: myTabs,
          indicatorColor: Theme.of(context).indicatorColor,
        ),
      ),
      drawer: AppDrawer(),
      body: TabBarView(
        controller: _tabController,
        children: myTabs.map((Tab tab) {
          final String label = tab.text;
          return label == featuredTab ? FeaturedList() : CategoryList();
        }).toList(),
      ),
      bottomNavigationBar: NavigationBar(id: Home.id),
      floatingActionButton: ARFloatingButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
