import 'package:agniture_flutter/screens/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:agniture_flutter/screens/product_detail/product_detail.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/screens/loading/loading.dart';
import 'package:provider/provider.dart';

class FeaturedList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final products = Provider.of<List<Product>>(context);
    return products == null
        ? Loading()
        : ListView(
            padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
            children: products.map<Widget>((Product product) {
              return Container(
                margin: const EdgeInsets.only(bottom: 8.0),
                child: FeaturedProductItem(product: product),
              );
            }).toList(),
          );
  }
}

class FeaturedProductItem extends StatelessWidget {
  const FeaturedProductItem({Key key, @required this.product})
      : assert(product != null),
        super(key: key);
  final Product product;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        App.analytics.logEvent(name: "view_item", properties: {
          'event_properties': {
            'itemId': product.id,
            'itemName': product.title,
            'itemCategory': product.category,
            'origin': "Featured List",
          }
        });

        Navigator.pushNamed(
          context,
          ProductDetail.id,
          arguments: DetailScreenArguments(product),
        );
      },
      child: Container(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 2, horizontal: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: SizedBox(
                  child: CachedNetworkImage(
                      imageUrl: product.image,
                      fit: BoxFit.fitWidth,
                      height: 180.0,
                      width: 500.0),
                ),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 8.0),
                  child: Text(product.title))
            ],
          ),
        ),
      ),
    );
  }
}
