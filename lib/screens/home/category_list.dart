import 'package:agniture_flutter/screens/app.dart';
import 'package:flutter/material.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/screens/products/products.dart';
import 'package:agniture_flutter/screens/home/categories.dart';

class CategoryList extends StatefulWidget {
  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<CategoryList> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.only(top: 10.0, left: 20.0, right: 20.0),
      children: categories.map<Widget>((ProductCategory category) {
        return Container(
          margin: const EdgeInsets.only(bottom: 20.0),
          child: CategoryItem(category: category),
        );
      }).toList(),
    );
  }
}

class CategoryItem extends StatelessWidget {
  const CategoryItem({Key key, @required this.category})
      : assert(category != null),
        super(key: key);
  final ProductCategory category;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        App.analytics.logEvent(name: "search", properties: {
          'event_properties': {
            'searchTerm': category.label,
            'origin': "Category",
          }
        });
        Navigator.pushNamed(
          context,
          Products.id,
          arguments: ProductsScreenArguments(category),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              spreadRadius: 0.5,
              blurRadius: 8.0,
              offset: Offset(3.0, 5.0),
            ),
          ],
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 25.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(category.label,
                  style: TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black87)),
              Image.asset(category.icon, fit: BoxFit.fitWidth, width: 28.0),
            ],
          ),
        ),
      ),
    );
  }
}
