import 'package:agniture_flutter/models/product.dart';

List<ProductCategory> categories = [
  ProductCategory(
      key: 'bedroom', label: 'Alcoba', icon: 'images/icons/icons8-room-96.png'),
  ProductCategory(
      key: 'bathroom',
      label: 'Baño',
      icon: 'images/icons/icons8-material-rounded-icons-96.png'),
  ProductCategory(
      key: 'decor',
      label: 'Decoración',
      icon: 'images/icons/icons8-wallpaper-96.png'),
  ProductCategory(
      key: 'kitchen',
      label: 'Cocina',
      icon: 'images/icons/icons8-kitchen-96.png'),
  ProductCategory(
      key: 'dinning-room',
      label: 'Comedor',
      icon: 'images/icons/icons8-dining-room-96.png'),
  ProductCategory(
      key: 'living-room',
      label: 'Sala',
      icon: 'images/icons/icons8-sofa-96.png'),
];
