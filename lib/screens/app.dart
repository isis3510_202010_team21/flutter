import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:amplitude_flutter/amplitude_flutter.dart';
import 'package:agniture_flutter/components/connectivity_state/connectivity_state.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/screens/products/products.dart';
import 'package:agniture_flutter/screens/shopping_summary/shopping_summary.dart';
import 'package:agniture_flutter/services/auth.dart';
import 'package:agniture_flutter/services/database.dart';
import 'package:agniture_flutter/services/moor_database.dart';
import 'package:agniture_flutter/screens/auth/auth.dart';
import 'package:agniture_flutter/models/user.dart';
import 'package:agniture_flutter/screens/landing/landing.dart';
import 'package:agniture_flutter/screens/home/home.dart';
import 'package:agniture_flutter/screens/product_detail/product_detail.dart';
import 'package:agniture_flutter/screens/favorites/favorites.dart';
import 'package:agniture_flutter/screens/search/search.dart';
import 'package:agniture_flutter/screens/shopping_cart/shopping_cart.dart';
import 'package:agniture_flutter/screens/aumented_reality/ar.dart';
import 'package:agniture_flutter/screens/pay/pay.dart';

class App extends StatelessWidget {
  // This widget is the root of your application.
  static AuthService authService = AuthService();
  static AmplitudeFlutter analytics =
      AmplitudeFlutter('3373cc5a6368a4735e2a21fb01683da0');
  @override
  Widget build(BuildContext context) {
    analytics.logEvent(name: "app_open");
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    final user = Provider.of<User>(context);
    return user != null ? AppProviders() : LandingApp();
  }
}

class AppProviders extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    return FutureProvider<List<Product>>(
      create: (_) => DatabaseService(uid: user.uid).getProducts(),
      child: StreamProvider<List<FavoriteItem>>.value(
          value: DatabaseService(uid: user.uid).favorites,
          child: Provider(create: (_) => AppDatabase(), child: LoggedApp())),
    );
  }
}

class LandingApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Augniture',
      home: Landing(),
      routes: <String, WidgetBuilder>{
        Login.id: (BuildContext context) => Login(),
        Register.id: (BuildContext context) => Register(),
      },
    );
  }
}

class LoggedApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Augniture',
      initialRoute: Home.id,
      home: ConnectivityState(),
      theme: ThemeData(
        primaryColor: Colors.white,
        indicatorColor: Color.fromRGBO(127, 127, 127, 1),
        accentColor: Colors.black,
      ),
      routes: <String, WidgetBuilder>{
        Login.id: (BuildContext context) => Login(),
        Register.id: (BuildContext context) => Register(),
        Home.id: (BuildContext context) => Home(),
        Favorites.id: (BuildContext context) => Favorites(),
        Search.id: (BuildContext context) => Search(),
        ShoppingCart.id: (BuildContext context) => ShoppingCart(),
        ProductDetail.id: (BuildContext context) => ProductDetail(),
        Products.id: (BuildContext context) => Products(),
        AumentedReality.id: (BuildContext context) => AumentedReality(),
        ShoppingSummary.id: (BuildContext context) => ShoppingSummary(),
        Pay.id: (BuildContext context) => Pay()
      },
    );
  }
}
