import 'package:flutter/material.dart';
import 'package:agniture_flutter/screens/favorites/favorites_list.dart';
import 'package:agniture_flutter/components/navigation_bar/navigation_bar.dart';
import 'package:agniture_flutter/components/ar_floating_button/ar_floating_button.dart';

class Favorites extends StatelessWidget {
  static const String id = '/favorites';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Favoritos',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0)),
      ),
      body: FavoritesList(),
      bottomNavigationBar: NavigationBar(id: id),
      floatingActionButton: ARFloatingButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
