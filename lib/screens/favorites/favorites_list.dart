import 'package:agniture_flutter/screens/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agniture_flutter/screens/product_detail/product_detail.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/screens/loading/loading.dart';

class FavoritesList extends StatelessWidget {
  List<FavoriteProduct> _referenceToModelList(
      List<Product> allProducts, List<FavoriteItem> references) {
    List<FavoriteProduct> result = [];
    references.forEach((favorite) {
      Product product = allProducts.firstWhere(
          (product) => product.id == favorite.product.documentID,
          orElse: () => null);
      if (product != null) {
        result.add(FavoriteProduct(id: favorite.id, product: product));
      }
    });
    return result;
  }

  @override
  Widget build(BuildContext context) {
    final products = Provider.of<List<Product>>(context);
    final favorites = Provider.of<List<FavoriteItem>>(context);

    bool loading = products == null || favorites == null;
    List<FavoriteProduct> favoriteProducts =
        loading ? [] : _referenceToModelList(products, favorites);
    return loading
        ? Loading()
        : Padding(
            padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10.0),
            child: _gridItemsRender(favoriteProducts));
  }

  Widget _gridItemsRender(favoriteProducts) {
    List<Widget> gridItems =
        favoriteProducts.map<Widget>((FavoriteProduct favorite) {
      return Container(
        margin: const EdgeInsets.only(bottom: 8.0),
        child: FavoriteProductItem(favoriteProduct: favorite),
      );
    }).toList();
    return GridView.count(
      primary: false,
      padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
      crossAxisSpacing: 20,
      mainAxisSpacing: 10,
      crossAxisCount: gridItems.length > 0 ? 2 : 1,
      childAspectRatio: 0.75,
      children: gridItems.length > 0
          ? gridItems
          : [
              Text(
                "No hay favoritos",
                textAlign: TextAlign.center,
              )
            ],
    );
  }
}

class FavoriteProductItem extends StatelessWidget {
  const FavoriteProductItem({Key key, @required this.favoriteProduct})
      : assert(favoriteProduct != null),
        super(key: key);
  final FavoriteProduct favoriteProduct;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        App.analytics.logEvent(name: "view_item", properties: {
          'event_properties': {
            'itemId': favoriteProduct.product.id,
            'itemName': favoriteProduct.product.title,
            'itemCategory': favoriteProduct.product.category,
            'origin': "Favorites List",
          }
        });
        Navigator.pushNamed(
          context,
          ProductDetail.id,
          arguments: DetailScreenArguments(favoriteProduct.product),
        );
      },
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: SizedBox(
                child: CachedNetworkImage(
                    imageUrl: favoriteProduct.product.image,
                    fit: BoxFit.fitHeight,
                    height: 150.0,
                    width: 200.0),
              ),
            ),
            Text(
              favoriteProduct.product.title,
              overflow: TextOverflow.fade,
              textAlign: TextAlign.end,
            )
          ],
        ),
      ),
    );
  }
}
