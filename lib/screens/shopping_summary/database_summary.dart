import 'package:agniture_flutter/screens/pay/pay.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/screens/product_detail/product_detail.dart';
import 'package:agniture_flutter/screens/loading/loading.dart';
import 'package:agniture_flutter/services/moor_database.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class DatabaseSummary extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Product> products = Provider.of<List<Product>>(context);
    AppDatabase database = Provider.of<AppDatabase>(context);
    bool loading = products == null || database == null;
    return loading
        ? Loading()
        : StreamBuilder(
            stream: database.watchAllShoppings(),
            builder: (context, AsyncSnapshot<List<Shopping>> snapshot) {
              final shoppings = snapshot.data ?? [];

              return ListView(
                padding:
                    const EdgeInsets.only(top: 8.0, left: 20.0, right: 20.0),
                children: _listItemsRender(shoppings, products, database),
              );
            });
  }

  List<Widget> _listItemsRender(
      List<Shopping> shoppings, List<Product> products, AppDatabase database) {
    int total = 0;
    List<Widget> listItems = [];
    shoppings.forEach((Shopping shopping) {
      ShoppingCartProduct shoppingCartProduct =
          _tableToModelProduct(products, shopping);
      if (shoppingCartProduct != null) {
        total +=
            shoppingCartProduct.quantity * shoppingCartProduct.product.price;
        listItems.add(Container(
          margin: const EdgeInsets.only(bottom: 8.0),
          child: ShoppingItem(
              shoppingItem: shoppingCartProduct,
              shoppingRef: shopping,
              database: database),
        ));
      }
    });
    // Total render
    listItems.add(_totalRender(total));
    // confirm shopping cart button
    listItems.add(ShoppingButton());
    // verify if there are more than 1 product
    return listItems.length > 2
        ? listItems
        : [
            Text(
              "El carrito de compras esta vacio",
              textAlign: TextAlign.center,
            )
          ];
  }

  ShoppingCartProduct _tableToModelProduct(
      List<Product> allProducts, Shopping shopping) {
    Product product = allProducts.firstWhere(
        (product) => product.id == shopping.product,
        orElse: () => null);
    if (product != null) {
      return ShoppingCartProduct(
          id: null, product: product, quantity: shopping.quantity);
    }
    return null;
  }

  Widget _totalRender(int total) {
    NumberFormat currency = NumberFormat.simpleCurrency();
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Total",
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
          Text(currency.format(total),
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold))
        ],
      ),
    );
  }
}

class ShoppingItem extends StatelessWidget {
  const ShoppingItem(
      {Key key,
      @required this.shoppingItem,
      @required this.shoppingRef,
      @required this.database})
      : assert(shoppingItem != null),
        assert(shoppingRef != null),
        assert(database != null),
        super(key: key);
  final ShoppingCartProduct shoppingItem;
  final Shopping shoppingRef;
  final AppDatabase database;

  @override
  Widget build(BuildContext context) {
    NumberFormat currency = NumberFormat.simpleCurrency();
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            spreadRadius: 0.5,
            blurRadius: 8.0,
            offset: Offset(3.0, 5.0),
          ),
        ],
      ),
      child: Padding(
        padding:
            EdgeInsets.only(top: 15.0, right: 15.0, bottom: 15.0, left: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
                child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        ProductDetail.id,
                        arguments: DetailScreenArguments(shoppingItem.product),
                      );
                    },
                    child: Text(shoppingItem.product.title,
                        overflow: TextOverflow.fade,
                        style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black87)))),
            Column(
              children: <Widget>[
                Text('Cant: ' + shoppingItem.quantity.toString(),
                    style: TextStyle(
                      fontSize: 16.0,
                    )),
                Text(currency.format(shoppingItem.product.price),
                    style: TextStyle(fontSize: 16.0))
              ],
            )
          ],
        ),
      ),
    );
  }
}

class ShoppingButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, Pay.id);
        },
        child: Container(
            padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: Colors.black,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Center(
                  child: Text(
                    "CONFIRMAR",
                    style: TextStyle(fontSize: 20.0, color: Colors.white),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                          size: 18,
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                          size: 18,
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                          size: 18,
                        ),
                      ],
                    )),
              ],
            )),
      ),
    );
  }
}
