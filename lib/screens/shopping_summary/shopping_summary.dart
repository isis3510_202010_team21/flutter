import 'package:agniture_flutter/screens/shopping_summary/database_summary.dart';
import 'package:flutter/material.dart';

class ShoppingSummary extends StatelessWidget {
  static const String id = '/shopping_summary';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Resumen de compras',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0)),
      ),
      body: DatabaseSummary(),
    );
  }
}
