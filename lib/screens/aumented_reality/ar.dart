import 'package:agniture_flutter/constans/constans.dart';
import 'package:flutter/material.dart';
import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/screens/product_detail/product_detail.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';

class AumentedReality extends StatefulWidget {
  /// Identificacion de la ruta
  static final String id = '/ar';
  @override
  _AumentedRealityState createState() => _AumentedRealityState();
}

class _AumentedRealityState extends State<AumentedReality> {
  /// Controlador de realidad virutal
  ArCoreController arCoreController;

  /// Producto cuyo modelo AR será mostrado
  Product producto;

  /// Permite determinar si la invocación del modelo AR se ha hecho desde la ventana del detalle o se obtuvo un modelo Random
  /// con el fin de eliminar la ventana o buscar el detalle si el producto es random, evitando ciclos infinitos.
  bool redirigidoDetalle;

  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    super.initState();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_closeARSession);
  }

  /// Obtiene todos los argumentos pasados por parametro para renderizar el modelo de AR
  void _getArguments(BuildContext context) {
    dynamic args = ModalRoute.of(context).settings.arguments;
    assert(args != null, "[AumentedReality] El producto a visualizar es nulo");
    this.producto = args[kArProduct];
    this.redirigidoDetalle = args[kArDetail];
    print("[AumentedReality] Producto cargado");
    print("[AumentedReality] ID: ${this.producto.id}");
    print("[AumentedReality] Titulo: ${this.producto.title}");
    print("[AumentedReality] Modelo: ${this.producto.arPath}");
  }

  Widget _displayDialog() {
    //Muestra el dialogo de ayuda para utilizar AR
    AlertDialog dialog = AlertDialog(
      title: const Text("Modelo del producto"),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Text(
              "Situe el telefono en una superficie plana con adecuada iluminacion hasta que detecte el plano"),
          SizedBox(height: 20),
          Image.asset('images/images/arPlaneExample.jpeg',
              alignment: Alignment.center),
        ],
      ),
      actions: <Widget>[
        FlatButton(
          child: const Text("De acuerdo"),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
    return dialog;
  }

  Future<void> _closeARSession(ConnectivityResult connection) async {
    if (connection == ConnectivityResult.none) { //No se tiene conexión a internet y debemos anular la ventana de AR
      AlertDialog perdidaConexionModelo = kDisplayDialog(
          "Error descargando el modelo",
          "Se ha perdido la conexión a internet y no se puede descargar el modelo. " + "Por favor intente mas tarde",
          Icons.error_outline,
          "DE ACUERDO",
          () {
            print("[CloseAR] Borrando las pantallas haciendo pop");
            int pagesToPop = 3;
            Navigator.popUntil(context, (_) => pagesToPop-- == 0);
          });
      showDialog(
          context: context,
          builder: (_) => perdidaConexionModelo,
          barrierDismissible: false
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    _getArguments(context);
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            this.producto.title,
            style: TextStyle(color: Colors.white),
          ),
          actions: <Widget>[
            IconButton(
                icon: const Icon(Icons.shopping_cart),
                tooltip: "Añadir el producto al carrito de compras",
                onPressed: () {
                  if (redirigidoDetalle) {
                    Navigator.pop(context);
                  } else {
                    //Cargar el detalle.
                    Navigator.pushReplacementNamed(context, ProductDetail.id,
                        arguments: DetailScreenArguments(this.producto));
                  }
                }),
            IconButton(
                icon: const Icon(Icons.info_outline),
                tooltip: "Funcionamiento de la vista de modelos",
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (_) => _displayDialog(),
                      barrierDismissible: false);
                }),
          ],
          backgroundColor: Colors.black,
        ),
        body: ArCoreView(
          onArCoreViewCreated: _onArCoreViewCreated,
          enableTapRecognizer: true,
        ),
      ),
    );
  }

  void _onArCoreViewCreated(ArCoreController controller) {
    arCoreController = controller;
    arCoreController.onPlaneTap = _handleOnPlaneTap;
  }

  void _addPiece(ArCoreHitTestResult plane) {
    final node = ArCoreReferenceNode(
        objectUrl: this.producto.arPath,
        position: plane.pose.translation,
        rotation: plane.pose.rotation);

    arCoreController.addArCoreNodeWithAnchor(node);
  }

  void _handleOnPlaneTap(List<ArCoreHitTestResult> hits) {
    final hit = hits.first;
    _addPiece(hit);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    arCoreController.dispose();
    super.dispose();
  }
}


