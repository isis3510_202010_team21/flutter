import 'package:agniture_flutter/screens/home/home.dart';
import 'package:flutter/material.dart';

class PaySuccess extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.asset(
          'images/icons/confirm.png',
          width: 50.0,
          height: 80.0,
        ),
        Text(
          "Pago exitoso",
          textAlign: TextAlign.start,
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        Text(
          "Gracias por tu compra",
          textAlign: TextAlign.start,
          style: TextStyle(fontSize: 18),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
          child: ContinueButton(),
        )
      ],
    );
  }
}

class ContinueButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamedAndRemoveUntil(context, Home.id, (r) => false);
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.black,
          ),
          child: Center(
            child: Text(
              "SEGUIR COMPRANDO",
              style: TextStyle(fontSize: 20.0, color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
