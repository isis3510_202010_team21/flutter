import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/models/user.dart';
import 'package:agniture_flutter/screens/pay/pay_success.dart';
import 'package:agniture_flutter/services/database.dart';
import 'package:agniture_flutter/services/moor_database.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Pay extends StatelessWidget {
  static const String id = '/pay';
  @override
  Widget build(BuildContext context) {
    return PayMethods();
  }
}

class PayMethods extends StatefulWidget {
  PayMethods({Key key}) : super(key: key);

  @override
  _PayMethodsState createState() => _PayMethodsState();
}

enum PayMethod { visa, master, spices }
enum PayStatus { paying, error, success }

class _PayMethodsState extends State<PayMethods> {
  PayMethod _method = PayMethod.visa;
  PayStatus _status = PayStatus.paying;

  Widget build(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    List<Product> products = Provider.of<List<Product>>(context);
    AppDatabase database = Provider.of<AppDatabase>(context);
    if (_status == PayStatus.success) return Scaffold(body: PaySuccess());

    return Scaffold(
      appBar: AppBar(
        title: Text('Método de pago',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0)),
      ),
      body: _payMethodsRender(context, user, products, database),
    );
  }

  Widget _payMethodsRender(context, user, products, database) {
    return Container(
      child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Text(
                "Seleccione un método de pago",
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 18),
              ),
              ListTile(
                title: Row(
                  children: <Widget>[
                    Image.asset(
                      'images/icons/visa.png',
                      width: 50.0,
                      height: 80.0,
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Text("Visa"))
                  ],
                ),
                leading: Radio(
                  value: PayMethod.visa,
                  groupValue: _method,
                  onChanged: (PayMethod value) {
                    setState(() {
                      _method = value;
                    });
                  },
                ),
              ),
              ListTile(
                title: Row(
                  children: <Widget>[
                    Image.asset(
                      'images/icons/mastercard.png',
                      width: 50.0,
                      height: 80.0,
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Text("Mastercard"))
                  ],
                ),
                leading: Radio(
                  value: PayMethod.master,
                  groupValue: _method,
                  onChanged: (PayMethod value) {
                    setState(() {
                      _method = value;
                    });
                  },
                ),
              ),
              ListTile(
                title: Row(
                  children: <Widget>[
                    Image.asset(
                      'images/icons/peach.png',
                      width: 50.0,
                      height: 80.0,
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Text("Especias"))
                  ],
                ),
                leading: Radio(
                  value: PayMethod.spices,
                  groupValue: _method,
                  onChanged: (PayMethod value) {
                    setState(() {
                      _method = value;
                    });
                  },
                ),
              ),
              _payButton(context, user, products, database),
            ],
          )),
    );
  }

  Widget _payButton(context, user, products, database) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: GestureDetector(
        onTap: () {
          _payProduct(context, user, products, database);
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.black,
          ),
          child: Center(
            child: Text(
              "PAGAR",
              style: TextStyle(fontSize: 20.0, color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _payProduct(context, user, products, database) async {
    List<Shopping> shoppings = await database.getAllShoppings();
    List<OrderItemId> orderList = [];
    shoppings.forEach((shopping) {
      if (productExist(products, shopping)) {
        orderList.add(OrderItemId(
            product: shopping.product, quantity: shopping.quantity));
      }
      _removePruduct(database, shopping);
    });
    DatabaseService(uid: user.uid).addOrder(orderList);
    setState(() {
      _status = PayStatus.success;
    });
  }

  Future<void> _removePruduct(database, shopping) async {
    return database.deleteShopping(shopping);
  }

  bool productExist(List<Product> allProducts, Shopping shopping) {
    Product product = allProducts.firstWhere(
        (product) => product.id == shopping.product,
        orElse: () => null);
    return product != null;
  }
}
