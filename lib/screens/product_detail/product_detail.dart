import 'package:agniture_flutter/screens/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agniture_flutter/services/database.dart';
import 'package:agniture_flutter/services/moor_database.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/models/user.dart';
import 'package:agniture_flutter/components/ar_floating_button/ar_floating_button.dart';
import 'package:agniture_flutter/components/navigation_bar/navigation_bar.dart';
import 'package:agniture_flutter/screens/shopping_cart/shopping_cart.dart';

class DetailScreenArguments {
  final Product product;
  DetailScreenArguments(
    this.product,
  );
}

class ProductDetail extends StatelessWidget {
  ProductDetail({Key key}) : super(key: key);
  static const String id = '/detail';
  FavoriteItem _favoriteProduct(List<FavoriteItem> favorites, Product product) {
    return favorites.firstWhere(
        (favorite) => favorite.product.documentID == product.id,
        orElse: () => null);
  }

  Future<void> _addTofavorites(
      String userId, String productId, Product product) {
    App.analytics.logEvent(name: "add_to_wishlist", properties: {
      'event_properties': {
        'itemId': product.id,
        'itemName': product.title,
        'itemCategory': product.category,
      }
    });
    return DatabaseService(uid: userId).addToFavorites(productId);
  }

  Future<void> _removefromFavorites(String userId, String favoriteId) {
    return DatabaseService(uid: userId).removeFromFavorites(favoriteId);
  }

  @override
  Widget build(BuildContext context) {
    final DetailScreenArguments args =
        ModalRoute.of(context).settings.arguments;

    final favorites = Provider.of<List<FavoriteItem>>(context);
    final user = Provider.of<User>(context);
    Product product = args.product;
    bool loading = user == null || favorites == null;
    FavoriteItem favorite =
        loading ? null : _favoriteProduct(favorites, product);
    bool isFavorite = favorite != null;

    return Scaffold(
      appBar: AppBar(
        title: Text(product.title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0)),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(
              right: 15.0,
            ),
            child: IconButton(
              icon: Icon(
                isFavorite ? Icons.favorite : Icons.favorite_border,
                color: isFavorite ? Colors.red : Colors.grey,
              ),
              onPressed: () {
                isFavorite
                    ? _removefromFavorites(user.uid, favorite.id)
                    : _addTofavorites(user.uid, product.id, product);
              },
            ),
          )
        ],
      ),
      body: ProductDetailBody(),
      bottomNavigationBar: NavigationBar(id: id),
      floatingActionButton: ARFloatingButton(producto: product),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

class ProductDetailBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final DetailScreenArguments args =
        ModalRoute.of(context).settings.arguments;
    Product product = args.product;
    return ListView(children: <Widget>[
      ClipRRect(
        borderRadius: BorderRadius.circular(0.0),
        child: SizedBox(
          child: CachedNetworkImage(
              imageUrl: product.image,
              fit: BoxFit.fitWidth,
              height: 250.0,
              width: 500.0),
        ),
      ),
      Padding(
        padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 22.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Descripción',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
            Container(
              height: 3,
              color: Theme.of(context).indicatorColor,
            ),
            Text(product.description),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 30.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ShoppingButton(
                    product.id,
                    product: product,
                  )
                ],
              ),
            )
          ],
        ),
      )
    ]);
  }
}

class ShoppingButton extends StatelessWidget {
  ShoppingButton(this.productId, {this.product});

  final String productId;
  final Product product;

  void _addToShoppingCart(context, database) async {
    List<Shopping> shoppings = await database.getAllShoppings();
    Shopping shopping = shoppings.firstWhere(
        (shopping) => shopping.product == productId,
        orElse: () => null);
    if (shopping != null) {
      database
          .updateShopping(shopping.copyWith(quantity: shopping.quantity + 1));
    } else {
      Shopping newShopping = Shopping(product: productId, quantity: 1);
      database.insertShopping(newShopping);
    }
    Navigator.pushNamed(context, ShoppingCart.id);
    App.analytics.logEvent(name: "add_to_cart", properties: {
      'event_properties': {
        'itemId': product.id,
        'itemName': product.title,
        'itemCategory': product.category,
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    AppDatabase database = Provider.of<AppDatabase>(context);
    return GestureDetector(
      onTap: () => _addToShoppingCart(context, database),
      child: Container(
          padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.black,
          ),
          child: Row(
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Icon(
                    Icons.shopping_cart,
                    color: Colors.white,
                  )),
              Center(
                child: Text(
                  "Agregar al Carrito",
                  style: TextStyle(fontSize: 20.0, color: Colors.white),
                ),
              ),
            ],
          )),
    );
  }
}
