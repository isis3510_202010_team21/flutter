import 'package:agniture_flutter/screens/app.dart';
import 'package:agniture_flutter/screens/home/home.dart';
import 'package:airplane_mode_detection/airplane_mode_detection.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:agniture_flutter/constans/constans.dart';
import 'package:flutter/services.dart';
import 'package:agniture_flutter/services/auth.dart';
import 'package:agniture_flutter/screens/loading/loading.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

/// Formulario para la recolección de datos de inicio de sesión o registro.
class _AuthScreenState extends State<AuthScreen> {
  bool loading = false;

  /// Email del usuario
  String email = '';

  /// Contraseña del usuario
  String password = '';

  /// Nombre del usuario
  String name = '';

  /// Permite determinar si el usuario hará login o register.
  Function executeFunction;

  /// Instancia de AuthServices, patron Singleton con objeto estatico de Home
  AuthService _authServices = App.authService;

  /// Permite lanzar una ventana de alerta para notificar al usuario
  void _notificarError(String pText) async {
    AlertDialog ventana = AlertDialog(
      title: Text('Atención'),
      content: Text(pText),
      actions: <Widget>[
        FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('OK'))
      ],
    );
    showDialog(
      context: context,
      builder: (_) => ventana,
      barrierDismissible: true,
    );
  }

  void _authConnect() async {
    if (!_verificarDatos(email, password))
      return; //Existen errores en el formulario.
    setState(() => loading = true);
    try {
      await executeFunction(email, password, name);
      Navigator.of(context).pushNamed(Home.id);
      App.analytics.logEvent(name: "login");
      setState(() => loading = false);
    } on PlatformException catch (e) {
      setState(() => loading = false);
      print(e);
      if (e.code == kFirebaseEmailFormat) {
        _notificarError('La dirección de correo no posee un formato valido.');
      } else if (e.code == kFirebaseWrongData ||
          e.code == kFirebaseEmailNotFound) {
        _notificarError(
            'La dirección de correo o la contraseña no son validas.');
      } else if (e.code == kFirebaseNetworkError) {
        String airplaneMode = await AirplaneModeDetection.detectAirplaneMode();
        if (airplaneMode == "ON") {
          _notificarError(
              'Sin conexión a Internet, Desactiva el modo avión para continuar.');
        } else {
          _notificarError('Sin conexión a Internet.');
        }
      }
    } catch (e) {
      setState(() => loading = false);
      print(e);
    }
  }

  bool _verificarDatos(String email, String password) {
    if (password.isEmpty || email.isEmpty) {
      _notificarError('Por favor complete todos los campos.');
      return false;
    } else if (password.length < 6) {
      _notificarError('La contraseña debe tener más de 6 caracteres.');
      return false;
    }
    return true;
  }

  /// Devuelve las entradas de texto que requerirá el formulario dependiendo de su tipo
  /// @param pText: Determina si el formulario es de registro o login
  /// PText  => [kRegisterTitle | kLoginTitle] del archivo de constantes.

  List<Widget> requiredTextFields(String pText) {
    if (pText == kRegisterTitle) {
      executeFunction = _authServices.registerUser;
    } else {
      executeFunction = _authServices.loginUser;
    }

    List<Widget> baseComponents = [
      TextField(
        maxLength: 50,
        autocorrect: false,
        cursorColor: Colors.white,
        keyboardType: TextInputType.emailAddress,
        textAlign: TextAlign.left,
        onChanged: (textTyped) {
          email = textTyped;
        },
        decoration: kTextFieldDecoration.copyWith(
            hintText: 'Ingresa tu email',
            hintStyle: TextStyle(color: Colors.white)),
        style: TextStyle(color: Colors.white),
      ),
      SizedBox(height: 10.0),
      TextField(
        maxLength: 50,
        autocorrect: false,
        obscureText: true,
        cursorColor: Colors.white,
        textAlign: TextAlign.left,
        onChanged: (textTyped) {
          password = textTyped;
        },
        decoration: kTextFieldDecoration.copyWith(
            hintText: 'Ingresa tu contraseña',
            hintStyle: TextStyle(color: Colors.white)),
        style: TextStyle(color: Colors.white),
      ),
    ];

    List<Widget> complements = [
      SizedBox(height: 10.0),
      TextField(
        maxLength: 50,
        autocorrect: false,
        cursorColor: Colors.white,
        keyboardType: TextInputType.emailAddress,
        textAlign: TextAlign.left,
        onChanged: (textTyped) {
          name = textTyped;
        },
        decoration: kTextFieldDecoration.copyWith(
            hintText: 'Ingresa tu nombre',
            hintStyle: TextStyle(color: Colors.white)),
        style: TextStyle(color: Colors.white),
      ),
    ];

    List<Widget> button = [
      SizedBox(height: 30.0),
      MainButton(
        text: pText,
        onPress: _authConnect,
      ),
      SizedBox(height: 30.0),
      IconButton(
          icon: Image.asset('images/icons/googleIcon.png'),
          onPressed: _authServices.googleAuth),
      SizedBox(height: 30.0),
      ChangeViewButton(
        text: pText,
      )
    ];

    if (pText == kRegisterTitle) {
      baseComponents.addAll(
          complements); //Agrega los demas campos para el formulario de registro
    }

    baseComponents.addAll(button);
    return baseComponents;
  }

  @override
  Widget build(BuildContext context) {
    final String text = ModalRoute.of(context).settings.arguments;
    assert(text == kLoginTitle || text == kRegisterTitle,
        'El titulo debe ser alguna de las dos constantes y NO puede ser nulo debe ser enviado por parametro.');
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.black12,
            body: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.person_pin,
                      color: Colors.white,
                      size: kAuthTitleSize,
                      semanticLabel: 'Titulo pantalla de Login o Register',
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    TypewriterAnimatedTextKit(
                      text: [text],
                      textStyle: TextStyle(
                          fontSize: kAuthTitleSize,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: requiredTextFields(text),
                ),
              ],
            ),
          );
  }
}

class MainButton extends StatelessWidget {
  MainButton({
    this.text,
    this.onPress,
  });
  final String text;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        width: 150.0,
        height: 40.0,
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.white,
        ),
        child: Center(
          child: Text(
            this.text,
            style: TextStyle(fontSize: 20.0),
          ),
        ),
      ),
    );
  }
}

class ChangeViewButton extends StatelessWidget {
  ChangeViewButton({
    this.text,
  });
  final String text;
  void _switchView(context) {
    if (text == kRegisterTitle) {
      Navigator.pushReplacementNamed(context, Login.id, arguments: kLoginTitle);
    } else {
      Navigator.pushReplacementNamed(context, Register.id,
          arguments: kRegisterTitle);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _switchView(context),
      child: Container(
        width: 200.0,
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Text(
            text == kRegisterTitle
                ? '¿Ya tienes una cuenta?'
                : '¿Aun no tienes una cuenta?',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 20.0,
                color: Colors.white70,
                decoration: TextDecoration.underline),
          ),
        ),
      ),
    );
  }
}

class Login extends AuthScreen {
  static const String id = '/login';
}

class Register extends AuthScreen {
  static const String id = '/register';
}
