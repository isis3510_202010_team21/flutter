import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:agniture_flutter/constans/constans.dart';
import 'package:agniture_flutter/screens/app.dart';
import 'package:agniture_flutter/services/auth.dart';
import 'package:airplane_mode_detection/airplane_mode_detection.dart';
import 'package:agniture_flutter/screens/auth/auth.dart';

class Landing extends StatelessWidget {
  Landing({Key key}) : super(key: key);
  static const String id = '/landing';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 200.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image(
                    image: AssetImage('images/images/landing-icon.png'),
                    width: 200,
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                MainButton('Login', Login.id, kLoginTitle),
                SizedBox(width: 10),
                MainButton('Registrarte', Register.id, kRegisterTitle),
              ],
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: SkipButton(),
            )
          ],
        ),
      ),
      backgroundColor: Colors.black,
    );
  }
}

class MainButton extends StatelessWidget {
  MainButton(this.text, this.route, [this.args]);
  final String text;
  final String route;
  final String args;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, this.route, arguments: this.args);
      },
      child: Container(
        width: 150.0,
        height: 40.0,
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.white,
        ),
        child: Center(
          child: Text(
            this.text,
            style: TextStyle(fontSize: 20.0),
          ),
        ),
      ),
    );
  }
}

class SkipButton extends StatelessWidget {
  final AuthService _authServices = App.authService;

  void _notificarError(context, pText) async {
    AlertDialog ventana = AlertDialog(
      title: Text('Atencion'),
      content: Text(pText),
      actions: <Widget>[
        FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('OK'))
      ],
    );
    showDialog(
      context: context,
      builder: (_) => ventana,
      barrierDismissible: true,
    );
  }

  void _authConnect(context) async {
    try {
      await _authServices.signInAnon();
    } on PlatformException catch (e) {
      print(e);
      if (e.code == kFirebaseNetworkError) {
        String airplaneMode = await AirplaneModeDetection.detectAirplaneMode();
        if (airplaneMode == "ON") {
          _notificarError(context,
              'Sin conexión a Internet, Desactiva el modo avión para continuar.');
        } else {
          _notificarError(context, 'Sin conexión a Internet');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _authConnect(context),
      child: Container(
        width: 150.0,
        height: 40.0,
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Text(
            'Saltar',
            style: TextStyle(
                fontSize: 20.0,
                color: Colors.white70,
                decoration: TextDecoration.underline),
          ),
        ),
      ),
    );
  }
}
