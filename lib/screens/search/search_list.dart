import 'package:agniture_flutter/screens/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:agniture_flutter/screens/product_detail/product_detail.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/screens/loading/loading.dart';
import 'package:provider/provider.dart';

class SearchList extends StatelessWidget {
  const SearchList({Key key, this.search}) : super(key: key);
  final String search;
  @override
  Widget build(BuildContext context) {
    List<Product> products = Provider.of<List<Product>>(context);
    List<Product> productsBySearch = search != null && search != ""
        ? products
            .where(
              (product) =>
                  product.title.toLowerCase().indexOf(search.toLowerCase()) !=
                  -1,
            )
            .toList()
        : products;

    return products == null ? Loading() : _gridItemsRender(productsBySearch);
  }

  Widget _gridItemsRender(products) {
    List<Widget> gridItems = products.map<Widget>((Product product) {
      return Container(
        margin: const EdgeInsets.only(bottom: 8.0),
        child: ProductItem(product: product),
      );
    }).toList();
    return GridView.count(
      primary: false,
      padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
      crossAxisSpacing: 20,
      mainAxisSpacing: 10,
      crossAxisCount: gridItems.length > 0 ? 2 : 1,
      childAspectRatio: 0.75,
      children: gridItems.length > 0
          ? gridItems
          : [
              Text(
                "No hay favoritos",
                textAlign: TextAlign.center,
              )
            ],
    );
  }
}

class ProductItem extends StatelessWidget {
  const ProductItem({Key key, @required this.product})
      : assert(product != null),
        super(key: key);
  final Product product;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        App.analytics.logEvent(name: "view_item", properties: {
          'event_properties': {
            'itemId': product.id,
            'itemName': product.title,
            'itemCategory': product.category,
            'origin': "Search List",
          }
        });

        Navigator.pushNamed(
          context,
          ProductDetail.id,
          arguments: DetailScreenArguments(product),
        );
      },
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: SizedBox(
                child: CachedNetworkImage(
                    imageUrl: product.image,
                    fit: BoxFit.fitHeight,
                    height: 150.0,
                    width: 200.0),
              ),
            ),
            Text(
              product.title,
              overflow: TextOverflow.fade,
              textAlign: TextAlign.end,
            )
          ],
        ),
      ),
    );
  }
}
