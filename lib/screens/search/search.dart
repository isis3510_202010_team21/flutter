import 'package:agniture_flutter/screens/app.dart';
import 'package:flutter/material.dart';
import 'package:agniture_flutter/screens/search/search_list.dart';
import 'package:agniture_flutter/components/ar_floating_button/ar_floating_button.dart';
import 'package:agniture_flutter/components/navigation_bar/navigation_bar.dart';
import 'package:agniture_flutter/constans/constans.dart';

class Search extends StatefulWidget {
  Search({Key key}) : super(key: key);
  static const String id = '/search';
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  String search = '';
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Buscar',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0)),
        bottom: PreferredSize(
            preferredSize: Size(0, 45),
            child: Padding(
              padding: EdgeInsets.all(10),
              child: TextField(
                maxLength: 50,
                textAlign: TextAlign.left,
                onChanged: (textTyped) {
                  App.analytics.logEvent(name: "search", properties: {
                    'event_properties': {
                      'searchTerm': textTyped,
                      'origin': "Search View",
                    }
                  });
                  setState(() {
                    search = textTyped;
                  });
                },
                decoration: kTextFieldDecorationBlack.copyWith(
                  hintText: '¿Qué quieres buscar?...',
                ),
              ),
            )),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: SearchList(search: search),
      ),
      bottomNavigationBar: NavigationBar(id: Search.id),
      floatingActionButton: ARFloatingButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
