import 'package:agniture_flutter/screens/app.dart';
import 'package:agniture_flutter/screens/aumented_reality/ar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agniture_flutter/models/product.dart';
import 'package:agniture_flutter/constans/constans.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:math' show Random;

class ARFloatingButton extends StatelessWidget {
  final Product producto;
  ARFloatingButton({this.producto});

  /// Obtiene un producto aleatorio en caso de que no se desee mostrar un modelo AR en especifico
  Product _getRandomModel(BuildContext context) {
    ///TODO Ojo con el punto de fallo: El delay de cargar la lista produce un bug y un null momentaneo
    ///TODO Ojo si se actualiza la lista, el solo escucha snapshot que tenia y solo para esta parte toca reiniciar, el paquete Provider fuerza a esto si se cambia el arbol, no me juzguen jajajajaja

    final List<Product> products =
        Provider.of<List<Product>>(context, listen: false);
    if (products == null) {
      return null;
    } else {
      Product random = products[Random().nextInt(products.length)];
      return random;
    }
  }

  /// Prepara los argumentos para pasarlos a la siguiente vista para ser renderizados
  _nextScreen(BuildContext context) async {
    var connectionStatus = await (Connectivity().checkConnectivity());
    if (connectionStatus == ConnectivityResult.none) {
      AlertDialog noNetwork = kDisplayDialog(
          "Error de red",
          "Actualmente no se posee conexión a internet y no se puede ejecutar la vista del modelo sin ello",
          Icons.error_outline,
          "OK",
          () => Navigator.pop(context));
      showDialog(
          context: context,
          builder: (_) => noNetwork,
          barrierDismissible: false);
    } else if (this.producto != null) {
      //Estamos en la vista de detalle.
      var args = {kArDetail: true, kArProduct: this.producto};
      App.analytics.logEvent(
        name: 'ar_model_view',
        properties: {
          'event_properties': {
            'itemId': this.producto.id,
            'itemName': this.producto.title,
            'itemCategory': this.producto.category,
            'originModelAR': "Detail view",
          }
        },
      );
      return Navigator.pushNamed(context, AumentedReality.id, arguments: args);
    } else {
      //Seleccion Aleatoria
      Product randomProduct = _getRandomModel(context);
      var args = {kArDetail: false, kArProduct: randomProduct};
      App.analytics.logEvent(
        name: 'ar_model_view',
        properties: {
          'event_properties': {
            'itemId': randomProduct.id,
            'itemName': randomProduct.title,
            'itemCategory': randomProduct.category,
            'originModelAR': "Random product",
          }
        },
      );
      AlertDialog random = kDisplayDialog(
          "¿Qué te parece este producto?",
          "Te recomendamos ver el siguiente producto. ¿Quieres verlo ahora?",
          Icons.info_outline,
          "De acuerdo",
          () =>
              Navigator.pushNamed(context, AumentedReality.id, arguments: args),
          "Luego",
          () => Navigator.pop(context));

      ///Mostrar la pantalla de dialogo.
      showDialog(
          context: context,
          builder: (_) => random, //El Widget de Alert Dialog
          barrierDismissible: false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        onPressed: () => _nextScreen(context),
        tooltip: 'Realidad Aumentada',
        child: Icon(
          Icons.camera_enhance,
          color: Theme.of(context).indicatorColor,
        ),
        backgroundColor: Colors.white);
  }
}
