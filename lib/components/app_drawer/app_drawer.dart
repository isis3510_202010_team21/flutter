import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:agniture_flutter/screens/app.dart';
import 'package:agniture_flutter/screens/auth/auth.dart';
import 'package:agniture_flutter/models/user.dart';
import 'package:agniture_flutter/constans/constans.dart';

class AppDrawer extends StatelessWidget {
  List<Widget> _renderDrawerList(context, user) {
    List<Widget> result = [
      DrawerHeader(
        decoration: BoxDecoration(
          color: Colors.black12,
        ),
        child: Text(
          user.name != null && user.name != '' ? user.name : 'Usuario',
          style: TextStyle(
            color: Colors.black,
            fontSize: 24,
          ),
        ),
      )
    ];

    if (user.email == null || user.email == '') {
      result.add(
        ListTile(
          onTap: () =>
              Navigator.of(context).pushNamed(Login.id, arguments: kLoginTitle),
          leading: Icon(Icons.cloud_queue),
          title: Text('Login'),
        ),
      );
      result.add(
        ListTile(
          onTap: () => Navigator.pushNamed(context, Register.id,
              arguments: kRegisterTitle),
          leading: Icon(Icons.person_add),
          title: Text('Regístrarte'),
        ),
      );
    } else {
      result.add(ListTile(
        onTap: () => App.authService.signOut(),
        leading: Icon(Icons.clear),
        title: Text('Cerrar Sesion'),
      ));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: _renderDrawerList(context, user),
      ),
    );
  }
}
