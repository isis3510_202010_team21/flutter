import 'package:flutter/material.dart';
import 'package:agniture_flutter/screens/home/home.dart';
import 'package:agniture_flutter/screens/favorites/favorites.dart';
import 'package:agniture_flutter/screens/search/search.dart';
import 'package:agniture_flutter/screens/shopping_cart/shopping_cart.dart';

class NavigationBar extends StatelessWidget {
  NavigationBar({Key key, this.id}) : super(key: key);
  final String id;
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.home,
                  color: id == Home.id ? Colors.black : Colors.grey,
                ),
                onPressed: id == Home.id
                    ? null
                    : () {
                        Navigator.pushNamed(
                          context,
                          Home.id,
                        );
                      },
              ),
              IconButton(
                icon: Icon(
                  Icons.search,
                  color: id == Search.id ? Colors.black : Colors.grey,
                ),
                onPressed: id == Search.id
                    ? null
                    : () {
                        Navigator.pushNamed(
                          context,
                          Search.id,
                        );
                      },
              ),
              SizedBox(
                width: 30,
              ),
              IconButton(
                icon: Icon(
                  Icons.bookmark,
                  color: id == Favorites.id ? Colors.black : Colors.grey,
                ),
                onPressed: id == Favorites.id
                    ? null
                    : () {
                        Navigator.pushNamed(
                          context,
                          Favorites.id,
                        );
                      },
              ),
              IconButton(
                icon: Icon(
                  Icons.shopping_cart,
                  color: id == ShoppingCart.id ? Colors.black : Colors.grey,
                ),
                onPressed: id == ShoppingCart.id
                    ? null
                    : () {
                        Navigator.pushNamed(
                          context,
                          ShoppingCart.id,
                        );
                      },
              ),
            ]));
  }
}
