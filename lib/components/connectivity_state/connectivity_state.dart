import 'dart:async';
import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import 'package:airplane_mode_detection/airplane_mode_detection.dart';

class ConnectivityState extends StatelessWidget {
  ConnectivityState({Key key, this.title}) : super(key: key);

  final String title;
  @override
  Widget build(BuildContext context) {
    return ConnectivityWrapper();
  }
}

class ConnectivityWrapper extends StatefulWidget {
  @override
  _ConnectivityWrapperState createState() => _ConnectivityWrapperState();
}

class _ConnectivityWrapperState extends State<ConnectivityWrapper> {
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    super.initState();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 200.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image(
                    image: AssetImage('images/images/landing-icon.png'),
                    width: 200,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.black,
    );
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    if (result == ConnectivityResult.none) {
      String airplaneMode = await AirplaneModeDetection.detectAirplaneMode();
      if (airplaneMode == "ON") {
        _showDialog(
            'Sin acceso a internet',
            'Algunas funcionalidades pueden no estar disponibles.'
                ' Desactiva el modo avión para continuar.');
      } else {
        _showDialog('Sin acceso a internet',
            'Algunas funcionalidades pueden no estar disponibles.');
      }
    }
  }

  void _showDialog(String title, String pText) async {
    AlertDialog ventana = AlertDialog(
      title: Text(title),
      content: Text(pText),
      actions: <Widget>[
        FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('OK'))
      ],
    );
    showDialog(
      context: context,
      builder: (_) => ventana,
      barrierDismissible: true,
    );
  }
}
