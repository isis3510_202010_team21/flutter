import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:agniture_flutter/models/user.dart';
import 'package:provider/provider.dart';

class UserIcon extends StatelessWidget {
  /// Función a ejecutar cuando se presione sobre el icono del usuario.
  @required
  final Function executeFunction;
  UserIcon({Key key, this.executeFunction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<User>(builder: (context, user, child) {
      Widget userCircleIcon; //Permite escoger entre un icono precargado o la imagen del perfil.

      if (user != null && user.photo != null) {
        userCircleIcon = CachedNetworkImage(
          imageUrl: user.photo,
          imageBuilder: (context, imageProvider) => CircleAvatar(
            radius: 25.0,
            backgroundImage: imageProvider,
          ),
          placeholder: (context, url) => CircularProgressIndicator(
            strokeWidth: 1,
            backgroundColor: Colors.black26,
          ),
          errorWidget: (context, url, error) => Icon(Icons.error),
        );
      }
      else {
        userCircleIcon = CircleAvatar(
          radius: 25.0,
          backgroundImage: AssetImage('images/images/iconUser.png'),
        );
      }

      return userCircleIcon;
    });
  }
}
