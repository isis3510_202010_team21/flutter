import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:agniture_flutter/screens/app.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agniture_flutter/models/user.dart';
import 'package:agniture_flutter/services/auth.dart';

void main() {
  runApp(new MyApp());
  //Habilitar Firestore
  final Firestore firestore = Firestore();
  firestore.settings(persistenceEnabled: true);
  //Habilitar Crashlytics
  Crashlytics.instance.enableInDevMode = true;
  FlutterError.onError = Crashlytics.instance.recordFlutterError;
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(value: AuthService().user, child: App());
  }
}
